app.factory('BikeService', function ($q, $http) {

var bikeStations = [];

var _bikeStations = [
  {"position": {
    "lat": 53.349562,
    "lng": -6.278198 }
  },
   {"position": {
    "lat": 53.349562,
    "lng": -6.265305 }
  },
   {"position": {
    "lat": 53.349562,
    "lng": -6.250427 }
  },
 { "position": {

    "lat": 53.339983,
    "lng": -6.295594

 }}
  
  ]


        return {

            all : function () {

                return bikeStations;

            },

            get : function ( id ) {


                    return bikeStations[id];
            },

            load : function () {
              //Set up angular promiseand defer
                var deferred = $q.defer();

                  var bikeServiceUrl = "https://api.jcdecaux.com/vls/v1/stations?contract=dublin&apiKey=";
                  var key = "4a3b1b035a651d4b9c22b1adc7c291936ab4989c";



       $http.get(bikeServiceUrl+key, {}).success( function (data) {


          console.log("loading", bikeServiceUrl+key, data);
           //bikeStations=[];
          angular.forEach(data, function(bikeStation) {
            
            //console.log(bikeStation)
            
            bikeStations.push(bikeStation);
            
                                    
          });

            //resolve the deferred promise so data is ultimatley sent back to caller
          deferred.resolve();

        }).error( function () {

              alert('there was an error');

        });


              return bikeStations;

            }



        }







});